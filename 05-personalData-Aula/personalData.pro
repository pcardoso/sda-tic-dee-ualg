#-------------------------------------------------
#
# Project created by QtCreator 2015-04-28T09:13:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = personalData
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    person.cpp

HEADERS  += mainwindow.h \
    person.h

FORMS    += mainwindow.ui
