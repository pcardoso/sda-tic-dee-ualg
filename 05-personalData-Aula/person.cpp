#include "person.h"


QString person::getName() const
{
    return name;
}

void person::setName(const QString &value)
{
    name = value;
}

QString person::getEmail() const
{
    return email;
}

void person::setEmail(const QString &value)
{
    email = value;
}

QString person::getObserv() const
{
    return observ;
}

void person::setObserv(const QString &value)
{
    observ = value;
}

person::person()
{
}

person::person(QString name, QString email, QString observ){
    setName(name);
    setEmail(email);
    setObserv(observ);
}
