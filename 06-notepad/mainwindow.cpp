#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionOpen_triggered()
{
    QString fn = QFileDialog::getOpenFileName(this, "open file...", "", "Text (*.txt) ;; All (*) ");

    if (fn != ""){
        ui->statusBar->showMessage("Opening: " + fn, 2000);
        openedFile = fn;
        QFile f(openedFile);
        f.open(QFile::ReadOnly | QFile::Text);
        ui->texto->setPlainText(f.readAll());
        f.close();
    }else{
        ui->statusBar->showMessage("No file selected", 2000);
    }



}

void MainWindow::on_actionSave_triggered()
{
    QString fn = "";

    if (openedFile == ""){
        fn = QFileDialog::getSaveFileName(this, "Save file...", "", "Text (*.txt) ;; All (*) ");
        if (fn != ""){
            openedFile = fn;
        }else{
            ui->statusBar->showMessage("No file selected" + openedFile, 2000);
        }
    }


    if ( openedFile != ""){
        ui->statusBar->showMessage("Saving to: " + openedFile, 2000);

        QFile f(openedFile);
        f.open(QFile::WriteOnly | QFile::Text);

        f.write( ui->texto->toPlainText().toUtf8());

        f.close();
    }


}

void MainWindow::on_actionNew_triggered()
{
    ui->texto->setPlainText("");
    openedFile = "";
}

void MainWindow::on_actionSave_as_triggered()
{
    QString fn = "";

    fn = QFileDialog::getSaveFileName(this, "Save file...", "", "Text (*.txt) ;; All (*) ");
    if (fn != ""){
        openedFile = fn;
        ui->statusBar->showMessage("Saving to: " + openedFile, 2000);

        QFile f(openedFile);
        f.open(QIODevice::WriteOnly | QIODevice::Text);

        f.write( ui->texto->toPlainText().toUtf8());

        f.close();
    }else{
        ui->statusBar->showMessage("No file selected" + openedFile, 2000);
    }
}
