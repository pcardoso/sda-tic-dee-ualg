#include "form.h"
#include "ui_form.h"

Form::Form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
}

Form::~Form()
{
    delete ui;
}


void Form::formReceiveData(int r, QStringList strLst){
    row = r;
    ui->nameTxt->setText(strLst[0]);
    ui->emailTxt->setText(strLst[1]);

}


void Form::on_pushButton_clicked()
{
    QStringList lst;
    lst.append(ui->nameTxt->text());
    lst.append(ui->emailTxt->text());

    emit retreiveData(row, lst);

    this->close();
}
