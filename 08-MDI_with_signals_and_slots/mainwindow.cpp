#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../db/data.sqlite3");
    db.open();

    filling = false;
    fillForm();
}


void MainWindow::fillForm(){
    int i;
    filling = true;
    QSqlQuery q = QSqlQuery(db);
    q.exec("SELECT * FROM Persons");
    q.first();
    for(i = 0; q.isValid(); ++i, q.next()){
        ui->personsTable->setRowCount(i + 1);
        ui->personsTable->setItem(i, 0, new QTableWidgetItem(q.value(0).toString()));
        ui->personsTable->setItem(i, 1, new QTableWidgetItem(q.value(1).toString()));
        ui->personsTable->setItem(i, 2, new QTableWidgetItem(q.value(2).toString()));
    }
    filling = false;
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_personsTable_cellDoubleClicked(int row, int column)
{
    form = new Form(this);
    form->setModal(true);
    form->show();

    connect(this, SIGNAL(sendData2Form(int, QStringList)), form, SLOT(formReceiveData(int, QStringList)));
    connect(form, SIGNAL(retreiveData(int, QStringList)), this, SLOT(receiveDataFromForm(int, QStringList)));


    QStringList strLst;
    strLst.append(ui->personsTable->item(row, 1)->text());
    strLst.append(ui->personsTable->item(row, 2)->text());
    emit sendData2Form(row, strLst);
}

void MainWindow::receiveDataFromForm(int r, QStringList ls){
    if (r != -1){
        QTableWidgetItem *item;
        item= ui->personsTable->item(r, 1);
        item->setText(ls[0]);
        item= ui->personsTable->item(r, 2);
        item->setText(ls[1]);

        QSqlQuery q;
        q.prepare("UPDATE Persons Set name = :n, email = :e WHERE id = :id");
        q.bindValue(":n", ls[0]);
        q.bindValue(":e", ls[1]);
        q.bindValue(":id", ui->personsTable->item(r, 0)->text().toInt());
        q.exec();
    }else{ // new
        QSqlQuery q;
        q.prepare("INSERT INTO Persons (name, email, birthdate) VALUES (:n, :e, '')"); // birthdate is a required field of the database. Not used in this example.
        q.bindValue(":n", ls[0]);
        q.bindValue(":e", ls[1]);

        if (!q.exec())
            qDebug() << q.lastError() ;

        fillForm(); //brute force!!!
    }
}

void MainWindow::on_newBt_clicked()
{
    form = new Form(this);
    form->setModal(true);
    form->show();

    connect(this, SIGNAL(sendData2Form(int, QStringList)), form, SLOT(formReceiveData(int, QStringList)));
    connect(form, SIGNAL(retreiveData(int, QStringList)), this, SLOT(receiveDataFromForm(int, QStringList)));


    QStringList strLst;
    strLst.append("");
    strLst.append("");
    emit sendData2Form(-1, strLst);
}
