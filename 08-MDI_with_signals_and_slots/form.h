#ifndef FORM_H
#define FORM_H

#include <QDialog>

namespace Ui {
class Form;
}

class Form : public QDialog
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = 0);
    ~Form();

private slots:
    void formReceiveData(int, QStringList);

    void on_pushButton_clicked();

signals:
    void retreiveData(int, QStringList);

private:
    Ui::Form *ui;
    int row;
};

#endif // FORM_H
