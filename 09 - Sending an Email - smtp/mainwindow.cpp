#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    host = "smtp.ualg.pt";
    port = 25;

    // ---- necessary to update
    user = "axxxx@ualg.pt";
    pw = "my secret!";
    // ----
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
     SmtpClient smtp(host, port, SmtpClient::TlsConnection);

     smtp.setUser(user);
     smtp.setPassword(pw);

     MimeMessage message;
     EmailAddress sender(ui->from->text(), ui->from->text());
     message.setSender(&sender);

     EmailAddress to(ui->to->text(), ui->to->text());
     message.addRecipient(&to);

     message.setSubject(ui->subject->text().toLatin1());

     // Now add some text to the email.
     // First we create a MimeText object.

     MimeText text;
     text.setText(ui->body->toPlainText());

     // Now add it to the mail
     message.addPart(&text);

     // Now we can send the mail
     if (!smtp.connectToHost()) {
         qDebug() << "Failed to connect to host!" << endl;
         return ;
     }

     if (!smtp.login()) {
         qDebug() << "Failed to login!" << endl;
         return ;
     }

     if (!smtp.sendMail(message)) {
         qDebug() << "Failed to send mail!" << smtp.getResponseCode() << endl;

         return ;
     }

     smtp.quit();
}
