#-------------------------------------------------
#
# Project created by QtCreator 2015-04-20T20:17:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 02-Copy_cut_and_Past_with_zero_lines_of_code
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
