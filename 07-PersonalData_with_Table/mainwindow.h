#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QDebug>
#include <QMessageBox>
#include <QComboBox>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_newBt_clicked();

    void on_tableWidget_cellChanged(int row, int column);

    void on_deleteBt_clicked();

    void on_actionExit_triggered();

    void on_actionInfo_triggered();

private:
    Ui::MainWindow *ui;

    QSqlDatabase db;

    void FillTable();

    bool filling;
};

#endif // MAINWINDOW_H
