#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../db/data.sqlite3");
    if (!db.open())
        qDebug() << "Error Opening db";


    FillTable();
}



MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::FillTable()
{
    filling = true;
    QSqlQuery q = QSqlQuery(db);

    q.exec("SELECT count(id) as nr FROM Persons");
    q.first();
    int n_r = q.value("nr").toInt();

    qDebug() << n_r;
    ui->tableWidget->setRowCount(n_r);

    q.exec("SELECT * FROM Persons");
    q.first();
    for(int r = 0; q.isValid(); q.next(), ++r){
        for(int c = 0; c < 4; ++c){
            ui->tableWidget->setItem(r, c, new QTableWidgetItem(QString(q.value(c).toString())));
        }
        QIcon qi = QIcon(QString(":/images/icons/flags/") + q.value("flag").toString());
        ui->tableWidget->setItem(r, 4, new QTableWidgetItem(qi, q.value("flag").toString()));

        QComboBox *qcb = new QComboBox();
        qcb->addItem("M");
        qcb->addItem("F");
        qcb->setCurrentText( q.value("sex").toString());
        ui->tableWidget->setCellWidget(r,5,qcb);

    }
    filling = false;
}


void MainWindow::on_newBt_clicked()
{
    QSqlQuery q(db);
    q.exec("INSERT INTO Persons (name, email, birthdate) Values ('','','')");
    FillTable();
}

void MainWindow::on_tableWidget_cellChanged(int row, int column)
{
    if (filling) return ;

    QSqlQuery q(db);

    q.prepare("UPDATE Persons SET name = :n, email = :e, birthdate = :b, flag = :f, sex = :s WHERE id = :id");
    q.bindValue(":n", ui->tableWidget->item(row,1)->text());
    q.bindValue(":e", ui->tableWidget->item(row,2)->text());
    q.bindValue(":b", ui->tableWidget->item(row,3)->text());
    q.bindValue(":f", ui->tableWidget->item(row,4)->text());
    //QComboBox *qcb = ui->tableWidget->item(row,5)->text();
    //ui->tableWidget->item(row, 5)->
    q.bindValue(":s", ui->tableWidget->item(row,5)->text());
    q.bindValue(":id", ui->tableWidget->item(row,0)->text().toInt());
    if (!q.exec()){
        qDebug() << q.lastError().text();
        qDebug() << q.lastQuery();
    }

    FillTable();
}

void MainWindow::on_deleteBt_clicked()
{
    if (1 == QMessageBox::question(this, "Delete?", "Delete the record?","yes", "no"))  return;

    int id = ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->text().toInt();
    QSqlQuery q(db);
    q.prepare("DELETE FROM Persons WHERE id = :id");
    q.bindValue(":id", id);
    if (!q.exec()){
        qDebug() << q.lastError().text();
        qDebug() << q.lastQuery();
        QMessageBox::critical(this, "Critical error", "Wrong query (see log)", "ok");
    }

    FillTable();
}

void MainWindow::on_actionInfo_triggered()
{
    QSqlQuery q(db);
    q.exec("SELECT count(id) as n_r FROM Persons");
    q.first();

    QMessageBox::information(this, "DB info", QString("Number of regs ") + q.value(0).toString(), "ok");
}
