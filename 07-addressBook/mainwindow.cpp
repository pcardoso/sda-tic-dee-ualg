#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/home/pcardoso/Documentos/Mega/Aulas/SDA - Sistemas de Desenvolvimento de Aplicações/Codigos/Codigo1415/07-addressBook/db/data.sqlite3");
    db.open();

    query = QSqlQuery("SELECT * FROM Persons");
    query.exec();

    if (query.isActive()){
        ui->statusBar->showMessage("Connection: OK", 5000);
        query.first();
        fillForm();
    }else{
        ui->statusBar->showMessage(query.lastError().text(), 5000);
    }
}

void MainWindow::fillForm(){
    if ( query.isValid()){
        ui->nameTxt->setText(query.value("name").toString());
        ui->emailTxt->setText(query.value("email").toString());
        ui->birthdateTxt->setDate(query.value("birthdate").toDate());
    }else{
        ui->nameTxt->setText("");
        ui->emailTxt->setText("");
        ui->birthdateTxt->setDate(QDate(2000, 1, 1));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    db.close();
}

void MainWindow::on_nextBt_clicked()
{
    query.next();
    if (!query.isValid()){
        query.first();
    }
    fillForm();
}

void MainWindow::on_previousBt_clicked()
{
    query.previous();
    if (! query.isValid())
        query.last();
    fillForm();
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    QString sql = QString("SELECT * FROM Persons WHERE name LIKE '%") +
                          ui->lineEdit->text() + "%'";
    ui->statusBar->showMessage(sql, 10000);
    query.exec(sql);
    query.first();
    fillForm();
}
