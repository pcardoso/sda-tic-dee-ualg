#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("./../db/data.sqlite3");

    if (!db.open())
        qDebug() << "Error opening db " + db.lastError().text();

    ui->tableWidget->hideColumn(0);

    fillingTable = false;
    FillTable();
}

void MainWindow::FillTable(){


    fillingTable = true;
    QSqlQuery q;
    q.prepare(  "SELECT count(id) as n FROM Persons WHERE name LIKE '%"
                        + ui->lineEdit->text().trimmed()
                        + "%'");
    q.bindValue(":s", ui->lineEdit->text().trimmed());

    if(! q.exec() ){
        qDebug() << "FillTable::Error: " + q.lastError().text();
        qDebug() << q.lastQuery() << ui->lineEdit->text().trimmed();
    }

    q.first();
    qDebug() << q.value("n").toString();
    ui->tableWidget->setRowCount(q.value("n").toInt());

    q.prepare("SELECT id, name, email, birthdate, flag FROM Persons WHERE name LIKE '%"
            + ui->lineEdit->text().trimmed()
            + "%'");
    q.bindValue(":s", ui->lineEdit->text());
    q.exec();
    q.first();
    for (int r = 0; q.isValid(); ++r, q.next()){
//        for(int c = 0; c < 4; c++){
//            ui->tableWidget->setItem(r,
//                                     c,
//                                     new QTableWidgetItem(
//                                            q.value(c).toString()
//                                         ));
//        }
        ui->tableWidget->setItem(r, 0, new QTableWidgetItem(q.value("id").toString()));
        ui->tableWidget->setItem(r, 1, new QTableWidgetItem(q.value("name").toString()));
        ui->tableWidget->setItem(r, 2, new QTableWidgetItem(q.value("email").toString()));
        ui->tableWidget->setItem(r, 3, new QTableWidgetItem(q.value("birthdate").toString()));
        ui->tableWidget->setItem(r, 4, new QTableWidgetItem(QIcon(":/flags/png/" + q.value("flag").toString()),
                                                                q.value("flag").toString()));

    }

    fillingTable = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tableWidget_cellEntered(int row, int column)
{
//    QMessageBox::information(this, "asm", "çlk");
}

void MainWindow::on_tableWidget_cellChanged(int row, int column)
{
    QSqlQuery q;

    if( ! fillingTable ){
        q.prepare("UPDATE Persons SET name = :n, email = :e, birthdate = :b, flag = :f WHERE id = :id");
        q.bindValue(":n", ui->tableWidget->item(row, 1)->text());
        q.bindValue(":e", ui->tableWidget->item(row, 2)->text());
        q.bindValue(":b", ui->tableWidget->item(row, 3)->text());
        q.bindValue(":f", ui->tableWidget->item(row, 4)->text());
        q.bindValue(":id", ui->tableWidget->item(row, 0)->text());
        q.exec();
        if (column == 4){
            fillingTable = true;
            QString f = ui->tableWidget->item(row, 4)->text();
            ui->tableWidget->setItem(row, 4,
                new QTableWidgetItem(QIcon(":/flags/png/" + f), f));
            fillingTable = false;

        }
    }
}

void MainWindow::on_newBt_clicked()
{
    QSqlQuery qqq;
    qqq.exec("INSERT INTO Persons (name, email, birthdate) VALUES ('a','','')");
    FillTable(); // força bruta!!! a melhorar!
}

void MainWindow::on_deleteBt_clicked()
{
    QSqlQuery q;
    int ans = QMessageBox::question(this, "Delete row...",
                          "Delete register?", "Yes", "No");
    if (ans == 0){  // yes - to delete
            q.prepare("DELETE FROM Persons WHERE id = :id");
            q.bindValue(":id", ui->tableWidget->item(
                            ui->tableWidget->currentRow(), 0
                        )->text()
                    );
            q.exec();
            FillTable(); // força bruta!!! a melhorar!
    }
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    FillTable();
}
